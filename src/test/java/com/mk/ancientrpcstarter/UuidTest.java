package com.mk.ancientrpcstarter;

import com.mk.ancientrpcstarter.logging.Log;
import com.mk.ancientrpcstarter.logging.LogFactory;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/18
 */

public class UuidTest {

    public static Log log = LogFactory.getLog(UuidTest.class);
    @Test
    public void test1() {
        System.out.println(UUID.randomUUID().toString());
    }
}
