package com.mk.ancientrpcstarter;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/23
 */

public class MethodTest {


    @Test
    public void test1() {
        try {
            Map<String, Method> map = new HashMap<>();
            map.put("sout", MethodTest.class.getMethod("testPrint", String.class));
            String s = "强哥万岁!!!";
            map.get("sout").invoke(MethodTest.class.newInstance(), s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testPrint(String s) {
        System.out.println(s);
    }


}
