package com.mk.ancientrpcstarter;

import com.mk.ancientrpcstarter.client.RpcClient;
import com.mk.ancientrpcstarter.server.TcpServer;
import org.junit.jupiter.api.Test;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/22
 */

public class TcpServerTest {

    @Test
    public void test1() {
        TcpServer server = new TcpServer(9999);
        try {
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(server);
    }

    @Test
    public void test2() throws Exception {
        RpcClient rpcClient = new RpcClient();
        rpcClient.doRequest();
    }
}
