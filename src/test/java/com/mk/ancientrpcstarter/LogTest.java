package com.mk.ancientrpcstarter;

import com.mk.ancientrpcstarter.logging.Log;
import com.mk.ancientrpcstarter.logging.LogFactory;
import org.junit.jupiter.api.Test;

/**
 * @description:
 * @author: M.K
 * @date: 2021/11/17
 */

public class LogTest {

    @Test
    public void test1() {
        Log logger = LogFactory.getLog(LogTest.class);
        logger.debug("aaaaaaaaaaa");
        logger.error("ccccccccccc");
    }

    @Test
    public void test2() {
        LogFactory.useSlf4jLogging();
        Log log = LogFactory.getLog(LogTest.class);
        logSomething(log);
    }

    private void logSomething(Log log) {
        log.warn("Warning message.");
        log.debug("Debug message.");
        log.error("Error message.");
        log.error("Error with Exception.", new Exception("Test exception."));
    }
}
