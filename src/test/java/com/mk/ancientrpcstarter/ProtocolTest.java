package com.mk.ancientrpcstarter;

import com.mk.ancientrpcstarter.enums.CommandEnum;
import com.mk.ancientrpcstarter.protocol.RpcProtocol;
import org.junit.jupiter.api.Test;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/19
 */

public class ProtocolTest {

    @Test
    public void test1() {

        RpcProtocol rpcProtocol = new RpcProtocol();
        rpcProtocol.setCmd(CommandEnum.REQUEST.getCode());
        rpcProtocol.setVersion(1);
        rpcProtocol.setBodyLen(100);
    }
}
