package com.mk.ancientrpcstarter.logging.stdout;

import com.mk.ancientrpcstarter.logging.Log;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/14
 */

public class StdOutImpl implements Log {

    public StdOutImpl(String clazz) {
        // TODO 这里要不要利用类名在打印时做格式化呢?????
    }

    @Override
    public boolean isDebugEnabled() {
        return true;
    }

    @Override
    public boolean isTraceEnabled() {
        return true;
    }

    @Override
    public void error(String s, Throwable e) {
        System.err.println(s);
        e.printStackTrace(System.err);
    }

    @Override
    public void error(String s) {
        System.err.println(s);
    }

    @Override
    public void debug(String s) {
        System.out.println(s);
    }

    @Override
    public void trace(String s) {
        System.out.println(s);
    }

    @Override
    public void warn(String s) {
        System.out.println(s);
    }
}
