package com.mk.ancientrpcstarter.logging;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/14
 */

public interface Log {
    boolean isDebugEnabled();

    boolean isTraceEnabled();

    void error(String s, Throwable e);

    void error(String s);

    void debug(String s);

    void trace(String s);

    void warn(String s);
}
