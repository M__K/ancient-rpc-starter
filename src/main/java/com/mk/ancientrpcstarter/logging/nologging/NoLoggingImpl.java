package com.mk.ancientrpcstarter.logging.nologging;

import com.mk.ancientrpcstarter.logging.Log;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/14
 */

public class NoLoggingImpl implements Log {

    public NoLoggingImpl(String clazz) {}

    @Override
    public boolean isDebugEnabled() {
        return false;
    }

    @Override
    public boolean isTraceEnabled() {
        return false;
    }

    @Override
    public void error(String s, Throwable e) {
        // 啥也不干
    }

    @Override
    public void error(String s) {
        // 啥也不干
    }

    @Override
    public void debug(String s) {
        // 啥也不干
    }

    @Override
    public void trace(String s) {
        // 啥也不干
    }

    @Override
    public void warn(String s) {
        // 啥也不干
    }
}
