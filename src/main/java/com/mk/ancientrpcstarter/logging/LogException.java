package com.mk.ancientrpcstarter.logging;

import com.mk.ancientrpcstarter.exceptions.PersistenceException;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/14
 */

public class LogException extends PersistenceException {

    public LogException() {
        super();
    }

    public LogException(String message) {
        super(message);
    }

    public LogException(String message, Throwable cause) {
        super(message, cause);
    }

    public LogException(Throwable cause) {
        super(cause);
    }

}
