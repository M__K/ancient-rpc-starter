package com.mk.ancientrpcstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/24
 */

@SpringBootApplication
public class AncientRpcStarterApplication {
    public static void main(String[] args) {
        SpringApplication.run(AncientRpcStarterApplication.class, args);
    }
}
