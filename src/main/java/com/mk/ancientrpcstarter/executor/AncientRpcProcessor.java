package com.mk.ancientrpcstarter.executor;

import com.mk.ancientrpcstarter.logging.Log;
import com.mk.ancientrpcstarter.logging.LogFactory;
import com.mk.ancientrpcstarter.properties.AncientRpcClientConfig;
import com.mk.ancientrpcstarter.properties.AncientRpcProperties;
import com.mk.ancientrpcstarter.properties.AncientRpcServerConfig;
import com.mk.ancientrpcstarter.server.RpcServer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.Objects;

/**
 * @description:
 * @author: M.K
 * @date: 2021/11/24
 */

public class AncientRpcProcessor implements ApplicationListener<ContextRefreshedEvent> {

    private static Log logger = LogFactory.getLog(AncientRpcProcessor.class);

    private AncientRpcServerConfig serverConfig;
    private AncientRpcClientConfig clientConfig;

    public AncientRpcProcessor(AncientRpcProperties properties) {
        this.serverConfig = properties.getServer();
        this.clientConfig = properties.getClient();
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (Objects.isNull(event.getApplicationContext().getParent())) {
            ApplicationContext context = event.getApplicationContext();
            // rpcServer启动
            startServer(context);
            // 注册调用方法
            injectService(context);
        }
    }

    private void startServer(ApplicationContext context) {
    }


    private void injectService(ApplicationContext context) {
    }
}
