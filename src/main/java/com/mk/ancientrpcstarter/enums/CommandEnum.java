package com.mk.ancientrpcstarter.enums;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/19
 */

public enum  CommandEnum {
    REQUEST(1),

    RESPONSE(2),

    FILE_UPLOAD(3),

    FILE_DOWNLOAD(4),
    ;

    private int code;

    CommandEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
