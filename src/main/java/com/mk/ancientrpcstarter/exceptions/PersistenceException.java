package com.mk.ancientrpcstarter.exceptions;

/**
 * @description:
 * @author: M.K
 * @date: 2021/11/17
 */

public class PersistenceException extends RuntimeException {

    public PersistenceException() {
        super();
    }

    public PersistenceException(String message) {
        super(message);
    }

    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersistenceException(Throwable cause) {
        super(cause);
    }
}
