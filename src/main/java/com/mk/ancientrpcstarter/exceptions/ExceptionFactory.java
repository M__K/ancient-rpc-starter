package com.mk.ancientrpcstarter.exceptions;

import com.mk.ancientrpcstarter.executor.ErrorContext;

/**
 * @description:
 * @author: M.K
 * @date: 2021/11/17
 */

public class ExceptionFactory {

    private ExceptionFactory() {
        // Prevent Instantiation
    }

    public static RuntimeException wrapException(String message, Exception e) {
        return new PersistenceException(ErrorContext.instance().message(message).cause(e).toString(), e);
    }

}
