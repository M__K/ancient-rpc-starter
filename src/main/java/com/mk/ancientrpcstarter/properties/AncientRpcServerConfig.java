package com.mk.ancientrpcstarter.properties;

/**
 * @description:
 * @author: M.K
 * @date: 2021/11/24
 */

public class AncientRpcServerConfig {

    private int port = 10010;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
