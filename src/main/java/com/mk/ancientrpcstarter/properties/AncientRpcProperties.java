package com.mk.ancientrpcstarter.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: M.K
 * @date: 2021/11/24
 */

@Component
@ConfigurationProperties(prefix = "ancient.rpc")
public class AncientRpcProperties {

    @NestedConfigurationProperty
    private AncientRpcServerConfig server = new AncientRpcServerConfig();

    @NestedConfigurationProperty
    private AncientRpcClientConfig client = new AncientRpcClientConfig();

    public AncientRpcServerConfig getServer() {
        return server;
    }

    public void setServer(AncientRpcServerConfig server) {
        this.server = server;
    }

    public AncientRpcClientConfig getClient() {
        return client;
    }

    public void setClient(AncientRpcClientConfig client) {
        this.client = client;
    }
}
