package com.mk.ancientrpcstarter.server;

import com.mk.ancientrpcstarter.logging.Log;
import com.mk.ancientrpcstarter.logging.LogFactory;
import com.mk.ancientrpcstarter.properties.AncientRpcServerConfig;

/**
 * @description:
 * @author: M.K
 * @date: 2021/11/25
 */

public class RpcServer {

    private static Log log = LogFactory.getLog(RpcServer.class);

    private AncientRpcServerConfig serverConfig;

    public AncientRpcServerConfig getServerConfig() {
        return serverConfig;
    }

    public void setServerConfig(AncientRpcServerConfig serverConfig) {
        this.serverConfig = serverConfig;
    }

    public RpcServer() {
        log.debug("Rpc Server init with no params");
    }

    public RpcServer(AncientRpcServerConfig ancientRpcServerConfig) {
        this.serverConfig = ancientRpcServerConfig;
        log.debug("Rpc Server init with ancientRpcServerConfig");
    }

    public void start() throws Exception {
        Thread tcpServerThread = new Thread("tcpServer") {
            @Override
            public void  run() {
                TcpServer tcpServer = new TcpServer(serverConfig);
                try {
                    tcpServer.start();
                } catch (Exception e) {
                    log.error("TcpServer start exception: " + e.getMessage());
                }
            }
        };
        tcpServerThread.start();
        tcpServerThread.join();
    }
}
