package com.mk.ancientrpcstarter.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/18
 */

public class RpcRequest implements Serializable {

    private String traceId;

    private Date sendDate;

    private String uri;

    private Object data;

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
