package com.mk.ancientrpcstarter.config;

import com.mk.ancientrpcstarter.executor.AncientRpcProcessor;
import com.mk.ancientrpcstarter.properties.AncientRpcProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @description:
 * @author: M.K
 * @date: 2021/11/24
 */

@Configuration
@EnableConfigurationProperties(AncientRpcProperties.class)
public class AncientRpcConfiguration {

    @Bean
    public AncientRpcProperties properties() {
        return new AncientRpcProperties();
    }

    @Bean
    public AncientRpcProcessor processor(@Autowired AncientRpcProperties properties) {
        return new AncientRpcProcessor(properties);
    }
}
