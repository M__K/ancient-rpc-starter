package com.mk.ancientrpcstarter.client;

import com.mk.ancientrpcstarter.entity.RpcRequest;
import com.mk.ancientrpcstarter.service.RequestService;

import java.util.Date;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/22
 */

public class RpcClient {

    public void doRequest() throws Exception {
        RpcRequest request = new RpcRequest();

        // 怼点属性到request里面
        request.setSendDate(new Date());
        request.setTraceId("66666666");
        request.setUri("/aa/cc");
        request.setData("hello RPC !");

        RequestService requestService = new RequestService();
        int ret = requestService.addObject(request);
        if(ret == 0) {
            System.out.println("调用远程服务创建用户成功！！！");
        }
        else {
            System.out.println("调用远程服务创建用户失败！！！");
        }
    }




}
