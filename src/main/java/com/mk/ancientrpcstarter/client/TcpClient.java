package com.mk.ancientrpcstarter.client;

import com.mk.ancientrpcstarter.logging.Log;
import com.mk.ancientrpcstarter.logging.LogFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/22
 */

public class TcpClient {

    private Log log = LogFactory.getLog(TcpClient.class);

    private static int MAX_PACKAGE_SIZE = 1024 * 4;
    private static String SERVER_IP = "127.0.0.1";
    private static int SERVER_PORT = 9999;
    private static TcpClient instance = null;

    private boolean isInit = false;
    //private ChannelFuture channelFuture = null;
    SocketChannel client = null;

    private final static int CONNECT_TIMEOUT_MILLIS = 2000;

    //private Bootstrap bootstrap = new Bootstrap();
    public TcpClient() {}

    public static TcpClient GetInstance() {
        if (instance == null) {
            instance = new TcpClient();
        }
        return instance;
    }


    public void init() throws Exception{
        if(!isInit) {
            client = SocketChannel.open(new InetSocketAddress(SERVER_IP, SERVER_PORT));
            client.configureBlocking(true);
        }
        isInit = true;
    }

    public boolean sendData(byte[] data){
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.put(data);
        byteBuffer.flip();
        int ret = 0;
        try {
            ret = client.write(byteBuffer);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public byte[] recvData()  {
        ByteBuffer byteBuffer = ByteBuffer.allocate(MAX_PACKAGE_SIZE);
        try {
            int rs = client.read(byteBuffer);
            byte[] bytes = new byte[rs];
            byteBuffer.flip();
            byteBuffer.get(bytes);
            return bytes;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
