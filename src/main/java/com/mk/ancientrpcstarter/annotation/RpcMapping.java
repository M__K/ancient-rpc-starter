package com.mk.ancientrpcstarter.annotation;

import com.mk.ancientrpcstarter.enums.CommandEnum;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/24
 */

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RpcMapping {

    String name() default "";

    @AliasFor("path")
    String[] value() default {};

    CommandEnum[] command() default {};
}
