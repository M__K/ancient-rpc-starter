package com.mk.ancientrpcstarter.annotation;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @description:
 * @author: M.K
 * @date: 2021/12/24
 */


@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Handler {
    @AliasFor(
            annotation = Component.class
    )
    String value() default "";
}
