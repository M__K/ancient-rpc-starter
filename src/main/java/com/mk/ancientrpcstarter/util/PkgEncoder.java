/**
 * @ClassName:     ${PkgEncoder}
 * @Description:   ${tcp编码器}
 * @author         ${wangzongsheng}
 * @version        V1.0
 * @Date           ${2019-01-14}
 */

package com.mk.ancientrpcstarter.util;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @description:
 * @author: M.K
 * @date: 2021/11/17
 */

public class PkgEncoder extends MessageToByteEncoder<Object>
{
    public PkgEncoder()
    {
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, Object msg, ByteBuf out) throws Exception
    {
        try {
            //在这之前可以实现编码工作。
            out.writeBytes((byte[])msg);
        }finally {

        }
    }
}